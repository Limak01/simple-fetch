clean:
	rm -f /usr/bin/sf

install: clean
	cargo build --release -vv
	cp target/release/simple_fetch /usr/bin/sf

uninstall:
	rm -f /usr/bin/sf


