# Simple Fetch

Simple system fetch written in rust.

![Screenshot](/screenshots/sf.png)

# Installation 
     
     git clone https://gitlab.com/Limak01/simple-fetch.git
     cd simple-fetch
     sudo make install

     # Removal
     sudo make uninstall

