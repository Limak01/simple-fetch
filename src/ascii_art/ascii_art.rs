use colored::Colorize;

pub fn tux() {
    println!("{}",r"    .--.".color("blue"));
    println!("{}",r"   |o_o |".color("blue"));
    println!("{}",r"   |:_/ |".color("blue"));
    println!("{}",r"  //   \ \".color("blue"));
    println!("{}",r" (|     | )".color("blue"));
    println!("{}",r"/'\_   _/`\".color("blue"));
    println!("{}",r"\___)=(___/".color("blue"));
    println!("");
}
