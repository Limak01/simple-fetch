use crate::info::utils;

pub fn get_hostname() -> String {
    utils::execute_command("hostnamectl", Some("--static"))
}
