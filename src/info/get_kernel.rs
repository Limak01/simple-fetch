use crate::info::utils;

pub fn get_kernel() -> String {
    utils::execute_command("uname", Some("-r"))
}
