use crate::info::{get_system::get_system, utils};

pub fn get_packages() -> String {
    let system = get_system();
    
    if system.contains("Arch") ||
       system.contains("Manjaro") {
        return utils::get_packman_pkgs().to_string()
    } 

    if system.contains("Ubuntu") || 
       system.contains("Debian") ||
       system.contains("Kali") || 
       system.contains("Zorin") || 
       system.contains("Mint")  ||
       system.contains("Pop") {
        return utils::get_dpkg_pkgs().to_string()
    }

    if system.contains("Fedora") {
      return utils::get_rpm_pkgs().to_string()
    }


    "Unknown".to_string()
}
