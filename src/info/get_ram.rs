use std::process::{ Command, Stdio };

pub fn get_ram() -> String {
    let args = vec!["-m3", "-Eo", "[0-9]{1,25}", "/proc/meminfo"];
    let mut result = Command::new("grep");

    for arg in args {
        result.arg(arg);
    }

    match result.stdout(Stdio::piped()).output() {
        Ok(value) => {
            let value = String::from_utf8(value.stdout).expect("Failed to fetch RAM data");
            let value: Vec<&str> = value.split("\n").collect();
            
            let total_memory: u32 = value[0].parse().expect("Failed conversion from &str to u32");
            let available_memory: u32 = value[2].parse().expect("Failed conversion from &str to u32");

            format!("{} | {} MiB", (total_memory - available_memory) / 1024, total_memory / 1024)
        },
        Err(err) => panic!("Failed to fetch RAM data, details: {}", err),
    }
}
