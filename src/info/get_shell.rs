use std::env;

pub fn get_shell() -> String {
    match env::var("SHELL") {
        Ok(result) => {
            let shell: Vec<&str> = result.split("/").collect();
            shell[shell.len() - 1].to_string()
        },
        Err(error) => panic!("Couldn't fetch shell, details: {}", error),
    }
} 
