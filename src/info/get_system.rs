use crate::info::utils;

pub fn get_system() -> String {
    utils::execute_command("grep", Some("-m1 -oP (?<=\").*?(?=\") /etc/os-release"))
}
