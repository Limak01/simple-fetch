use std::env;

pub fn get_terminal() -> String{
    match env::var("TERM") {
        Ok(terminal) => {
            let terminal: Vec<&str> = terminal.split("-").collect();
            terminal[terminal.len() - 1].to_string()
        },
        Err(error) => panic!("Failed fetching terminal name, details: {}", error),
    }
}
