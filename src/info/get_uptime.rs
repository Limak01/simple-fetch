use crate::info::utils;

pub fn get_uptime() -> String {
    utils::execute_command("uptime", Some("-p"))
}
