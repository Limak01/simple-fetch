use crate::info::utils;

pub fn get_username() -> String {
    utils::execute_command("whoami", None)
}
