use std::process::{Command, Stdio};
use std::fs;


pub fn execute_command(command: &str, args: Option<&str>) -> String {
    let mut cmd = Command::new(command);
    
    match args {
        Some(arguments) => {
            let arguments: Vec<&str> = arguments.split(" ").collect();

            for argument in arguments {
                cmd.arg(argument);
            }
        },
        None => (),
    }
    

    match cmd.stdout(Stdio::piped()).output() {
        Ok(value) => { 
            let output = String::from_utf8(value.stdout).expect("");
            output.replace("\n", "")
        },
        Err(error) => panic!("Failed to fetch data, details: {}", error),
    }
}

// Packages
pub fn get_packman_pkgs() -> usize {
    let pkgs = fs::read_dir("/var/lib/pacman/local").expect("Failed to fetch packages");

    pkgs.count()
}

pub fn get_dpkg_pkgs() -> usize {
    let cmd = Command::new("dpkg").arg("--get-selections").stdout(Stdio::piped()).output();

    let pkgs = match cmd {
        Ok(value) => String::from_utf8(value.stdout).expect("Failed to fetch dpkg packages"),
        Err(err) => panic!("Failed to fetch dpkg packages, details: {}", err),
    };

    pkgs.split("\n").count() 
}

pub fn get_rpm_pkgs() -> usize {
    let cmd = Command::new("rpm").arg("-qa").stdout(Stdio::piped()).output();

    let pkgs = match cmd {
        Ok(value) => String::from_utf8(value.stdout).expect("Failed to fetch rpm packages"),
        Err(err) => panic!("Failed to fetch rpm packages, details: {}", err),
    };

    pkgs.split("\n").count()
}
