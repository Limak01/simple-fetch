pub mod ascii_art;
pub mod info;

use colored::Colorize;

use ascii_art::ascii_art::tux;
use info::get_username::get_username;
use info::get_hostname::get_hostname;
use info::get_system::get_system;
use info::get_kernel::get_kernel;
use info::get_uptime::get_uptime;
use info::get_shell::get_shell;
use info::get_terminal::get_terminal;
use info::get_packages::get_packages;
use info::get_ram::get_ram;

fn main() {
    let info = [
        ("User", get_username()),
        ("Hostname", get_hostname()),
        ("Distribution", get_system()),
        ("Kernel", get_kernel()),
        ("Uptime", get_uptime()),
        ("Shell", get_shell()),
        ("Terminal", get_terminal()),
        ("Packages", get_packages()),
        ("Memory", get_ram()),
    ];

   tux(); 
        
    for (name, output) in info {
        println!("{}: {}", name.blue().bold(), output);
    }

    println!();
}
